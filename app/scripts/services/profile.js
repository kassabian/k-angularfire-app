'use strict';

app.factory('Profile', ['$window', 'FIREBASE_URL', '$firebase', 'Post', '$q', function ($window, FIREBASE_URL, $firebase, Post, $q) {
  var ref = new $window.Firebase(FIREBASE_URL);

  var profile = {
    get: function(userId) {
      return $firebase(ref.child('profile').child(userId)).$asObject();
    },

    getPosts: function(userId) {
      var deferred = $q.defer();

      $firebase(ref.child('user_posts').child(userId)).$asArray().$loaded().then(function (data) {
        var posts = {};

        for(var i = 0; i < data.length; i++) {
          var value = data[i].$value;
          posts[value] = Post.get(value);
        }
        deferred.resolve(posts);
      });

      return deferred.promise;
    }
  };

  return profile;
}]);
